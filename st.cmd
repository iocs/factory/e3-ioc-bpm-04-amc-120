require essioc
require adsis8300bpm

# set macros
epicsEnvSet("CONTROL_GROUP",  "PBI-BPM04")
epicsEnvSet("AMC_NAME",       "Ctrl-AMC-120")
epicsEnvSet("AMC_DEVICE",     "/dev/sis8300-5")
epicsEnvSet("EVR_NAME",       "PBI-BPM04:Ctrl-EVR-101:")
epicsEnvSet("SYSTEM1_PREFIX", "MBL-030LWU:PBI-BPM-001:")
epicsEnvSet("SYSTEM1_NAME",   "MBL 030 LWU BPM 01")
epicsEnvSet("SYSTEM2_PREFIX", "MBL-040LWU:PBI-BPM-001:")
epicsEnvSet("SYSTEM2_NAME",   "MBL 040 LWU BPM 01")

# load BPM
iocshLoad("$(adsis8300bpm_DIR)/bpm-main.iocsh", "CONTROL_GROUP=$(CONTROL_GROUP), AMC_NAME=$(AMC_NAME), AMC_DEVICE=$(AMC_DEVICE), EVR_NAME=$(EVR_NAME)")

# load common module
iocshLoad $(essioc_DIR)/common_config.iocsh

